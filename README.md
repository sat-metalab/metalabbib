# metalabbib

This repository provides a bibtex file containing publications by the SAT Metalab team. Most papers are available in the "papers" directory. 

It also contains a generic slide presentation of the Metalab.

## Build

You may build the bibliography of your choice by changing the line:

```
\addbibresource{metalab-papers.bib}
```

in the `print_bib.tex` file and then running:

```
latexmk print_bib.tex
```

in a Terminal window.
