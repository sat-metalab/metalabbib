#!/bin/bash

# 'set -e' makes the script fail if a command fails
set -e

latexmk print_bib.tex
